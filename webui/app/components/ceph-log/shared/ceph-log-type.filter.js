"use strict";

export default () => {
  return (value) => {

    if (value === "POST") {
      return "create";
    }
    if (value === "DELETE") {
      return "delete";
    }
    if (value === "PUT") {
      return "change";
    }
    if (value === "INFO") {
      return "success";
    }
    if (value === "ERROR") {
      return "failure";
    }
    if (value === "LOGIN") {
      return "login";
    }
    if (value === "LOGOUT") {
      return "logout";
    }
    return "";
  };
};
